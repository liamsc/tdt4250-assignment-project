# Tdt4250 assignment

Welcome to the TDT4250 assignment of:

 * Andreas Jensen Jonassen
 * Liam Svanåsbakken Crouch

## Overview

This repository corresponds to an eclipse workspace. The projects contain various aspects of the project:

 * `tdt4250-assignment` - The emf models and implementation code
 * `tdt4250-assignment.tests` - Unit tests for the afformentioned project


## Assumptions made

 * All study programmes start in the fall of a year
 * A year always has two semesters(the number of semesters in a programme is always even)
 * Course groups can only span one semester. 
 * A study cannot change type(master -> bachelor, etc) from year to year.

## Read more

 * [tdt4250-assignment has its own readme](tdt4250-assignment/README.md), check it out!