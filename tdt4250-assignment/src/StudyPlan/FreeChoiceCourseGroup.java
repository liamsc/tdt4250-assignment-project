/**
 */
package StudyPlan;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Free Choice Course Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.FreeChoiceCourseGroup#isGuaranteedNonColliding <em>Guaranteed Non Colliding</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getFreeChoiceCourseGroup()
 * @model
 * @generated
 */
public interface FreeChoiceCourseGroup extends CourseGroup {
	/**
	 * Returns the value of the '<em><b>Guaranteed Non Colliding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guaranteed Non Colliding</em>' attribute.
	 * @see #setGuaranteedNonColliding(boolean)
	 * @see StudyPlan.StudyPlanPackage#getFreeChoiceCourseGroup_GuaranteedNonColliding()
	 * @model
	 * @generated
	 */
	boolean isGuaranteedNonColliding();

	/**
	 * Sets the value of the '{@link StudyPlan.FreeChoiceCourseGroup#isGuaranteedNonColliding <em>Guaranteed Non Colliding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guaranteed Non Colliding</em>' attribute.
	 * @see #isGuaranteedNonColliding()
	 * @generated
	 */
	void setGuaranteedNonColliding(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getName();

} // FreeChoiceCourseGroup
