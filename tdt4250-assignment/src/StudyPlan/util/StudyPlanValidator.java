/**
 */
package StudyPlan.util;

import StudyPlan.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see StudyPlan.StudyPlanPackage
 * @generated
 */
public class StudyPlanValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final StudyPlanValidator INSTANCE = new StudyPlanValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "StudyPlan";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyPlanValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return StudyPlanPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case StudyPlanPackage.PROGRAMME:
				return validateProgramme((Programme)value, diagnostics, context);
			case StudyPlanPackage.ACADEMIC_YEAR:
				return validateAcademicYear((AcademicYear)value, diagnostics, context);
			case StudyPlanPackage.SEMESTER:
				return validateSemester((Semester)value, diagnostics, context);
			case StudyPlanPackage.SPECIALIZATION:
				return validateSpecialization((Specialization)value, diagnostics, context);
			case StudyPlanPackage.COURSE:
				return validateCourse((Course)value, diagnostics, context);
			case StudyPlanPackage.COURSE_GROUP:
				return validateCourseGroup((CourseGroup)value, diagnostics, context);
			case StudyPlanPackage.DEPARTMENT:
				return validateDepartment((Department)value, diagnostics, context);
			case StudyPlanPackage.STUDY:
				return validateStudy((Study)value, diagnostics, context);
			case StudyPlanPackage.OBLIGATORY_PICK_COURSE_GROUP:
				return validateObligatoryPickCourseGroup((ObligatoryPickCourseGroup)value, diagnostics, context);
			case StudyPlanPackage.FREE_CHOICE_COURSE_GROUP:
				return validateFreeChoiceCourseGroup((FreeChoiceCourseGroup)value, diagnostics, context);
			case StudyPlanPackage.ESEMESTER:
				return validateESemester((ESemester)value, diagnostics, context);
			case StudyPlanPackage.EPROGRAM_TYPE:
				return validateEProgramType((EProgramType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProgramme(Programme programme, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(programme, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validateProgramme_needsSameAmountOfYearsAsProgrammeType(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validateProgramme_specializationsMustHaveCorrectLength(programme, diagnostics, context);
		if (result || diagnostics != null) result &= validateProgramme_correctNumberOfBaseYears(programme, diagnostics, context);
		return result;
	}

	/**
	 * Validates the needsSameAmountOfYearsAsProgrammeType constraint of '<em>Programme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateProgramme_needsSameAmountOfYearsAsProgrammeType(Programme programme, DiagnosticChain diagnostics, Map<Object, Object> context) {
		int lengthOfProgramme = programme.getCommonYears().size();
		if (programme.getSpecializations().size() > 0) {
			lengthOfProgramme += programme.getSpecializations().get(0).getYears().size();
		}
		if (programme.getStudy().getDurationYears() != lengthOfProgramme) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "needsSameAmountOfYearsAsProgrammeType", getObjectLabel(programme, context) },
						 new Object[] { programme },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the specializationsMustHaveCorrectLength constraint of '<em>Programme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateProgramme_specializationsMustHaveCorrectLength(Programme programme, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean aSpecializationIsInvalid = false;
		for(Specialization specialization : programme.getSpecializations()) {
			if(programme.getStudy().getDurationYears() - programme.getSpecializationStartYearOffset() != specialization.getYears().size()) {
				aSpecializationIsInvalid = true;
				break;
			}
		}
		
		if (aSpecializationIsInvalid) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "specializationsMustHaveCorrectLength", getObjectLabel(programme, context) },
						 new Object[] { programme },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the correctNumberOfBaseYears constraint of '<em>Programme</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateProgramme_correctNumberOfBaseYears(Programme programme, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (programme.getCommonYears().size() != programme.getSpecializationStartYearOffset()) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "correctNumberOfBaseYears", getObjectLabel(programme, context) },
						 new Object[] { programme },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAcademicYear(AcademicYear academicYear, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(academicYear, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(academicYear, diagnostics, context);
		if (result || diagnostics != null) result &= validateAcademicYear_cannotHaveTwoSemestersOfSameSeason(academicYear, diagnostics, context);
		return result;
	}

	/**
	 * Validates the cannotHaveTwoSemestersOfSameSeason constraint of '<em>Academic Year</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateAcademicYear_cannotHaveTwoSemestersOfSameSeason(AcademicYear academicYear, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean invalid = false;
		
		if (academicYear.getSemesters().size() != ESemester.VALUES.size()) {
			invalid = true;
		}
		
		
		for(ESemester semesterSeason : ESemester.VALUES) {
			boolean found = false;
			for(Semester semester : academicYear.getSemesters()) {
				if (semester.getSemesterSeason() == semesterSeason) {
					found = true;
				}
			}
			if(!found) {
				invalid = true;
				break;
			}
		}
		
		if (invalid) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "cannotHaveTwoSemestersOfSameSeason", getObjectLabel(academicYear, context) },
						 new Object[] { academicYear },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSemester(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(semester, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validateSemester_maxOneObligatoryGroup(semester, diagnostics, context);
		if (result || diagnostics != null) result &= validateSemester_maxOneOfEachFreeChoiceGroup(semester, diagnostics, context);
		return result;
	}

	/**
	 * Validates the maxOneObligatoryGroup constraint of '<em>Semester</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateSemester_maxOneObligatoryGroup(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		int numberOfObligatoryGroups = 0;
		for(CourseGroup group : semester.getCourseGroups()) {
			if (group instanceof ObligatoryPickCourseGroup && ((ObligatoryPickCourseGroup) group).getCourseCount() == 0) {
				numberOfObligatoryGroups += 1;
			}
		}
		
		if (numberOfObligatoryGroups > 1) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "maxOneObligatoryGroup", getObjectLabel(semester, context) },
						 new Object[] { semester },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the maxOneOfEachFreeChoiceGroup constraint of '<em>Semester</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc ->
	 * @generated NOT
	 */
	public boolean validateSemester_maxOneOfEachFreeChoiceGroup(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean foundGuaranteedNonColliding = false;
		boolean foundNotGuaranteedNonColliding = false;
		
		for(CourseGroup group : semester.getCourseGroups()) {
			if (group instanceof FreeChoiceCourseGroup) {
				boolean isGuaranteedNonColliding = ((FreeChoiceCourseGroup) group).isGuaranteedNonColliding();
				if (isGuaranteedNonColliding && !foundGuaranteedNonColliding) {
					foundGuaranteedNonColliding = true;
				} else if (!isGuaranteedNonColliding && !foundNotGuaranteedNonColliding) {
					foundNotGuaranteedNonColliding = true;
				} else {
					if (diagnostics != null) {
						diagnostics.add
							(createDiagnostic
								(Diagnostic.ERROR,
								 DIAGNOSTIC_SOURCE,
								 0,
								 "_UI_GenericConstraint_diagnostic",
								 new Object[] { "maxOneOfEachFreeChoiceGroup", getObjectLabel(semester, context) },
								 new Object[] { semester },
								 context));
					}
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Validates the mustBeAbleToAchieve30Credits constraint of '<em>Semester</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateSemester_mustBeAbleToAchieve30Credits(Semester semester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (semester.getMaxCredits() < 30) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "mustBeAbleToAchieve30Credits", getObjectLabel(semester, context) },
						 new Object[] { semester },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSpecialization(Specialization specialization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(specialization, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(course, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseGroup(CourseGroup courseGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(courseGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStudy(Study study, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(study, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObligatoryPickCourseGroup(ObligatoryPickCourseGroup obligatoryPickCourseGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(obligatoryPickCourseGroup, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validateObligatoryPickCourseGroup_isValidCourseCountRequirement(obligatoryPickCourseGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validateObligatoryPickCourseGroup_hasEnoughCoursesForItsOwnConstraint(obligatoryPickCourseGroup, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValidCourseCountRequirement constraint of '<em>Obligatory Pick Course Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateObligatoryPickCourseGroup_isValidCourseCountRequirement(ObligatoryPickCourseGroup obligatoryPickCourseGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (obligatoryPickCourseGroup.getCourseCount() < 0) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "isValidCourseCountRequirement", getObjectLabel(obligatoryPickCourseGroup, context) },
						 new Object[] { obligatoryPickCourseGroup },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the hasEnoughCoursesForItsOwnConstraint constraint of '<em>Obligatory Pick Course Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateObligatoryPickCourseGroup_hasEnoughCoursesForItsOwnConstraint(ObligatoryPickCourseGroup obligatoryPickCourseGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (obligatoryPickCourseGroup.getCourses().size() < obligatoryPickCourseGroup.getCourseCount()) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "hasEnoughCoursesForItsOwnConstraint", getObjectLabel(obligatoryPickCourseGroup, context) },
						 new Object[] { obligatoryPickCourseGroup },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFreeChoiceCourseGroup(FreeChoiceCourseGroup freeChoiceCourseGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(freeChoiceCourseGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateESemester(ESemester eSemester, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEProgramType(EProgramType eProgramType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //StudyPlanValidator
