/**
 */
package StudyPlan;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see StudyPlan.StudyPlanFactory
 * @model kind="package"
 * @generated
 */
public interface StudyPlanPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "StudyPlan";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/tdt4250-assignment/model/studyplan.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "StudyPlan";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StudyPlanPackage eINSTANCE = StudyPlan.impl.StudyPlanPackageImpl.init();

	/**
	 * The meta object id for the '{@link StudyPlan.impl.ProgrammeImpl <em>Programme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.ProgrammeImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getProgramme()
	 * @generated
	 */
	int PROGRAMME = 0;

	/**
	 * The feature id for the '<em><b>Start Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAMME__START_YEAR = 0;

	/**
	 * The feature id for the '<em><b>Common Years</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAMME__COMMON_YEARS = 1;

	/**
	 * The feature id for the '<em><b>Specializations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAMME__SPECIALIZATIONS = 2;

	/**
	 * The feature id for the '<em><b>Specialization Start Year Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAMME__SPECIALIZATION_START_YEAR_OFFSET = 3;

	/**
	 * The feature id for the '<em><b>Study</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAMME__STUDY = 4;

	/**
	 * The number of structural features of the '<em>Programme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAMME_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Programme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAMME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.AcademicYearImpl <em>Academic Year</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.AcademicYearImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getAcademicYear()
	 * @generated
	 */
	int ACADEMIC_YEAR = 1;

	/**
	 * The feature id for the '<em><b>Programme</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACADEMIC_YEAR__PROGRAMME = 0;

	/**
	 * The feature id for the '<em><b>Start Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACADEMIC_YEAR__START_YEAR = 1;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACADEMIC_YEAR__SPECIALIZATION = 2;

	/**
	 * The feature id for the '<em><b>Semesters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACADEMIC_YEAR__SEMESTERS = 3;

	/**
	 * The number of structural features of the '<em>Academic Year</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACADEMIC_YEAR_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Academic Year</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACADEMIC_YEAR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.SemesterImpl <em>Semester</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.SemesterImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getSemester()
	 * @generated
	 */
	int SEMESTER = 2;

	/**
	 * The feature id for the '<em><b>Course Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__COURSE_GROUPS = 0;

	/**
	 * The feature id for the '<em><b>Year</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__YEAR = 1;

	/**
	 * The feature id for the '<em><b>Semester Season</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__SEMESTER_SEASON = 2;

	/**
	 * The feature id for the '<em><b>Max Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__MAX_CREDITS = 3;

	/**
	 * The number of structural features of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.SpecializationImpl <em>Specialization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.SpecializationImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getSpecialization()
	 * @generated
	 */
	int SPECIALIZATION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Programme</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZATION__PROGRAMME = 1;

	/**
	 * The feature id for the '<em><b>Years</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZATION__YEARS = 2;

	/**
	 * The number of structural features of the '<em>Specialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Specialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.CourseImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 4;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 2;

	/**
	 * The feature id for the '<em><b>Running Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RUNNING_SEMESTER = 3;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__DURATION = 4;

	/**
	 * The feature id for the '<em><b>Credits Per Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS_PER_SEMESTER = 5;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.CourseGroupImpl <em>Course Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.CourseGroupImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getCourseGroup()
	 * @generated
	 */
	int COURSE_GROUP = 5;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_GROUP__SEMESTER = 0;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_GROUP__COURSES = 1;

	/**
	 * The feature id for the '<em><b>Max Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_GROUP__MAX_CREDITS = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_GROUP__NAME = 3;

	/**
	 * The number of structural features of the '<em>Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_GROUP_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.DepartmentImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 6;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 1;

	/**
	 * The feature id for the '<em><b>Studies</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__STUDIES = 2;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.StudyImpl <em>Study</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.StudyImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getStudy()
	 * @generated
	 */
	int STUDY = 7;

	/**
	 * The feature id for the '<em><b>Programmes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__PROGRAMMES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__NAME = 1;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__CODE = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__TYPE = 3;

	/**
	 * The feature id for the '<em><b>Duration Years</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY__DURATION_YEARS = 4;

	/**
	 * The number of structural features of the '<em>Study</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Study</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.ObligatoryPickCourseGroupImpl <em>Obligatory Pick Course Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.ObligatoryPickCourseGroupImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getObligatoryPickCourseGroup()
	 * @generated
	 */
	int OBLIGATORY_PICK_COURSE_GROUP = 8;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP__SEMESTER = COURSE_GROUP__SEMESTER;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP__COURSES = COURSE_GROUP__COURSES;

	/**
	 * The feature id for the '<em><b>Max Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP__MAX_CREDITS = COURSE_GROUP__MAX_CREDITS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP__NAME = COURSE_GROUP__NAME;

	/**
	 * The feature id for the '<em><b>Course Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP__COURSE_COUNT = COURSE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Obligatory Pick Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP_FEATURE_COUNT = COURSE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP___GET_NAME = COURSE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Obligatory Pick Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATORY_PICK_COURSE_GROUP_OPERATION_COUNT = COURSE_GROUP_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link StudyPlan.impl.FreeChoiceCourseGroupImpl <em>Free Choice Course Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.impl.FreeChoiceCourseGroupImpl
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getFreeChoiceCourseGroup()
	 * @generated
	 */
	int FREE_CHOICE_COURSE_GROUP = 9;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP__SEMESTER = COURSE_GROUP__SEMESTER;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP__COURSES = COURSE_GROUP__COURSES;

	/**
	 * The feature id for the '<em><b>Max Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP__MAX_CREDITS = COURSE_GROUP__MAX_CREDITS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP__NAME = COURSE_GROUP__NAME;

	/**
	 * The feature id for the '<em><b>Guaranteed Non Colliding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP__GUARANTEED_NON_COLLIDING = COURSE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Free Choice Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP_FEATURE_COUNT = COURSE_GROUP_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP___GET_NAME = COURSE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Free Choice Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_CHOICE_COURSE_GROUP_OPERATION_COUNT = COURSE_GROUP_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link StudyPlan.ESemester <em>ESemester</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.ESemester
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getESemester()
	 * @generated
	 */
	int ESEMESTER = 10;

	/**
	 * The meta object id for the '{@link StudyPlan.EProgramType <em>EProgram Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.EProgramType
	 * @see StudyPlan.impl.StudyPlanPackageImpl#getEProgramType()
	 * @generated
	 */
	int EPROGRAM_TYPE = 11;


	/**
	 * Returns the meta object for class '{@link StudyPlan.Programme <em>Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Programme</em>'.
	 * @see StudyPlan.Programme
	 * @generated
	 */
	EClass getProgramme();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Programme#getStartYear <em>Start Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Year</em>'.
	 * @see StudyPlan.Programme#getStartYear()
	 * @see #getProgramme()
	 * @generated
	 */
	EAttribute getProgramme_StartYear();

	/**
	 * Returns the meta object for the containment reference list '{@link StudyPlan.Programme#getCommonYears <em>Common Years</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Common Years</em>'.
	 * @see StudyPlan.Programme#getCommonYears()
	 * @see #getProgramme()
	 * @generated
	 */
	EReference getProgramme_CommonYears();

	/**
	 * Returns the meta object for the containment reference list '{@link StudyPlan.Programme#getSpecializations <em>Specializations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Specializations</em>'.
	 * @see StudyPlan.Programme#getSpecializations()
	 * @see #getProgramme()
	 * @generated
	 */
	EReference getProgramme_Specializations();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Programme#getSpecializationStartYearOffset <em>Specialization Start Year Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specialization Start Year Offset</em>'.
	 * @see StudyPlan.Programme#getSpecializationStartYearOffset()
	 * @see #getProgramme()
	 * @generated
	 */
	EAttribute getProgramme_SpecializationStartYearOffset();

	/**
	 * Returns the meta object for the container reference '{@link StudyPlan.Programme#getStudy <em>Study</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Study</em>'.
	 * @see StudyPlan.Programme#getStudy()
	 * @see #getProgramme()
	 * @generated
	 */
	EReference getProgramme_Study();

	/**
	 * Returns the meta object for class '{@link StudyPlan.AcademicYear <em>Academic Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Academic Year</em>'.
	 * @see StudyPlan.AcademicYear
	 * @generated
	 */
	EClass getAcademicYear();

	/**
	 * Returns the meta object for the container reference '{@link StudyPlan.AcademicYear#getProgramme <em>Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Programme</em>'.
	 * @see StudyPlan.AcademicYear#getProgramme()
	 * @see #getAcademicYear()
	 * @generated
	 */
	EReference getAcademicYear_Programme();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.AcademicYear#getStartYear <em>Start Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Year</em>'.
	 * @see StudyPlan.AcademicYear#getStartYear()
	 * @see #getAcademicYear()
	 * @generated
	 */
	EAttribute getAcademicYear_StartYear();

	/**
	 * Returns the meta object for the container reference '{@link StudyPlan.AcademicYear#getSpecialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Specialization</em>'.
	 * @see StudyPlan.AcademicYear#getSpecialization()
	 * @see #getAcademicYear()
	 * @generated
	 */
	EReference getAcademicYear_Specialization();

	/**
	 * Returns the meta object for the containment reference list '{@link StudyPlan.AcademicYear#getSemesters <em>Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Semesters</em>'.
	 * @see StudyPlan.AcademicYear#getSemesters()
	 * @see #getAcademicYear()
	 * @generated
	 */
	EReference getAcademicYear_Semesters();

	/**
	 * Returns the meta object for class '{@link StudyPlan.Semester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Semester</em>'.
	 * @see StudyPlan.Semester
	 * @generated
	 */
	EClass getSemester();

	/**
	 * Returns the meta object for the container reference '{@link StudyPlan.Semester#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Year</em>'.
	 * @see StudyPlan.Semester#getYear()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_Year();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Semester#getMaxCredits <em>Max Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Credits</em>'.
	 * @see StudyPlan.Semester#getMaxCredits()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_MaxCredits();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Semester#getSemesterSeason <em>Semester Season</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester Season</em>'.
	 * @see StudyPlan.Semester#getSemesterSeason()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_SemesterSeason();

	/**
	 * Returns the meta object for class '{@link StudyPlan.Specialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Specialization</em>'.
	 * @see StudyPlan.Specialization
	 * @generated
	 */
	EClass getSpecialization();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Specialization#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see StudyPlan.Specialization#getName()
	 * @see #getSpecialization()
	 * @generated
	 */
	EAttribute getSpecialization_Name();

	/**
	 * Returns the meta object for the container reference '{@link StudyPlan.Specialization#getProgramme <em>Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Programme</em>'.
	 * @see StudyPlan.Specialization#getProgramme()
	 * @see #getSpecialization()
	 * @generated
	 */
	EReference getSpecialization_Programme();

	/**
	 * Returns the meta object for the containment reference list '{@link StudyPlan.Specialization#getYears <em>Years</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Years</em>'.
	 * @see StudyPlan.Specialization#getYears()
	 * @see #getSpecialization()
	 * @generated
	 */
	EReference getSpecialization_Years();

	/**
	 * Returns the meta object for the containment reference list '{@link StudyPlan.Semester#getCourseGroups <em>Course Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Course Groups</em>'.
	 * @see StudyPlan.Semester#getCourseGroups()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_CourseGroups();

	/**
	 * Returns the meta object for class '{@link StudyPlan.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see StudyPlan.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see StudyPlan.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see StudyPlan.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see StudyPlan.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Course#getRunningSemester <em>Running Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Running Semester</em>'.
	 * @see StudyPlan.Course#getRunningSemester()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_RunningSemester();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Course#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see StudyPlan.Course#getDuration()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Duration();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Course#getCreditsPerSemester <em>Credits Per Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits Per Semester</em>'.
	 * @see StudyPlan.Course#getCreditsPerSemester()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_CreditsPerSemester();

	/**
	 * Returns the meta object for class '{@link StudyPlan.CourseGroup <em>Course Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Group</em>'.
	 * @see StudyPlan.CourseGroup
	 * @generated
	 */
	EClass getCourseGroup();

	/**
	 * Returns the meta object for the container reference '{@link StudyPlan.CourseGroup#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Semester</em>'.
	 * @see StudyPlan.CourseGroup#getSemester()
	 * @see #getCourseGroup()
	 * @generated
	 */
	EReference getCourseGroup_Semester();

	/**
	 * Returns the meta object for the reference list '{@link StudyPlan.CourseGroup#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Courses</em>'.
	 * @see StudyPlan.CourseGroup#getCourses()
	 * @see #getCourseGroup()
	 * @generated
	 */
	EReference getCourseGroup_Courses();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.CourseGroup#getMaxCredits <em>Max Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Credits</em>'.
	 * @see StudyPlan.CourseGroup#getMaxCredits()
	 * @see #getCourseGroup()
	 * @generated
	 */
	EAttribute getCourseGroup_MaxCredits();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.CourseGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see StudyPlan.CourseGroup#getName()
	 * @see #getCourseGroup()
	 * @generated
	 */
	EAttribute getCourseGroup_Name();

	/**
	 * Returns the meta object for class '{@link StudyPlan.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see StudyPlan.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the containment reference list '{@link StudyPlan.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see StudyPlan.Department#getCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Courses();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see StudyPlan.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the containment reference '{@link StudyPlan.Department#getStudies <em>Studies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Studies</em>'.
	 * @see StudyPlan.Department#getStudies()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Studies();

	/**
	 * Returns the meta object for class '{@link StudyPlan.Study <em>Study</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study</em>'.
	 * @see StudyPlan.Study
	 * @generated
	 */
	EClass getStudy();

	/**
	 * Returns the meta object for the containment reference list '{@link StudyPlan.Study#getProgrammes <em>Programmes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Programmes</em>'.
	 * @see StudyPlan.Study#getProgrammes()
	 * @see #getStudy()
	 * @generated
	 */
	EReference getStudy_Programmes();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Study#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see StudyPlan.Study#getName()
	 * @see #getStudy()
	 * @generated
	 */
	EAttribute getStudy_Name();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Study#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see StudyPlan.Study#getCode()
	 * @see #getStudy()
	 * @generated
	 */
	EAttribute getStudy_Code();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Study#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see StudyPlan.Study#getType()
	 * @see #getStudy()
	 * @generated
	 */
	EAttribute getStudy_Type();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.Study#getDurationYears <em>Duration Years</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration Years</em>'.
	 * @see StudyPlan.Study#getDurationYears()
	 * @see #getStudy()
	 * @generated
	 */
	EAttribute getStudy_DurationYears();

	/**
	 * Returns the meta object for class '{@link StudyPlan.ObligatoryPickCourseGroup <em>Obligatory Pick Course Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obligatory Pick Course Group</em>'.
	 * @see StudyPlan.ObligatoryPickCourseGroup
	 * @generated
	 */
	EClass getObligatoryPickCourseGroup();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.ObligatoryPickCourseGroup#getCourseCount <em>Course Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Course Count</em>'.
	 * @see StudyPlan.ObligatoryPickCourseGroup#getCourseCount()
	 * @see #getObligatoryPickCourseGroup()
	 * @generated
	 */
	EAttribute getObligatoryPickCourseGroup_CourseCount();

	/**
	 * Returns the meta object for the '{@link StudyPlan.ObligatoryPickCourseGroup#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see StudyPlan.ObligatoryPickCourseGroup#getName()
	 * @generated
	 */
	EOperation getObligatoryPickCourseGroup__GetName();

	/**
	 * Returns the meta object for class '{@link StudyPlan.FreeChoiceCourseGroup <em>Free Choice Course Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Choice Course Group</em>'.
	 * @see StudyPlan.FreeChoiceCourseGroup
	 * @generated
	 */
	EClass getFreeChoiceCourseGroup();

	/**
	 * Returns the meta object for the attribute '{@link StudyPlan.FreeChoiceCourseGroup#isGuaranteedNonColliding <em>Guaranteed Non Colliding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guaranteed Non Colliding</em>'.
	 * @see StudyPlan.FreeChoiceCourseGroup#isGuaranteedNonColliding()
	 * @see #getFreeChoiceCourseGroup()
	 * @generated
	 */
	EAttribute getFreeChoiceCourseGroup_GuaranteedNonColliding();

	/**
	 * Returns the meta object for the '{@link StudyPlan.FreeChoiceCourseGroup#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see StudyPlan.FreeChoiceCourseGroup#getName()
	 * @generated
	 */
	EOperation getFreeChoiceCourseGroup__GetName();

	/**
	 * Returns the meta object for enum '{@link StudyPlan.ESemester <em>ESemester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ESemester</em>'.
	 * @see StudyPlan.ESemester
	 * @generated
	 */
	EEnum getESemester();

	/**
	 * Returns the meta object for enum '{@link StudyPlan.EProgramType <em>EProgram Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EProgram Type</em>'.
	 * @see StudyPlan.EProgramType
	 * @generated
	 */
	EEnum getEProgramType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StudyPlanFactory getStudyPlanFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link StudyPlan.impl.ProgrammeImpl <em>Programme</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.ProgrammeImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getProgramme()
		 * @generated
		 */
		EClass PROGRAMME = eINSTANCE.getProgramme();

		/**
		 * The meta object literal for the '<em><b>Start Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROGRAMME__START_YEAR = eINSTANCE.getProgramme_StartYear();

		/**
		 * The meta object literal for the '<em><b>Common Years</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAMME__COMMON_YEARS = eINSTANCE.getProgramme_CommonYears();

		/**
		 * The meta object literal for the '<em><b>Specializations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAMME__SPECIALIZATIONS = eINSTANCE.getProgramme_Specializations();

		/**
		 * The meta object literal for the '<em><b>Specialization Start Year Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROGRAMME__SPECIALIZATION_START_YEAR_OFFSET = eINSTANCE.getProgramme_SpecializationStartYearOffset();

		/**
		 * The meta object literal for the '<em><b>Study</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAMME__STUDY = eINSTANCE.getProgramme_Study();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.AcademicYearImpl <em>Academic Year</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.AcademicYearImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getAcademicYear()
		 * @generated
		 */
		EClass ACADEMIC_YEAR = eINSTANCE.getAcademicYear();

		/**
		 * The meta object literal for the '<em><b>Programme</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACADEMIC_YEAR__PROGRAMME = eINSTANCE.getAcademicYear_Programme();

		/**
		 * The meta object literal for the '<em><b>Start Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACADEMIC_YEAR__START_YEAR = eINSTANCE.getAcademicYear_StartYear();

		/**
		 * The meta object literal for the '<em><b>Specialization</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACADEMIC_YEAR__SPECIALIZATION = eINSTANCE.getAcademicYear_Specialization();

		/**
		 * The meta object literal for the '<em><b>Semesters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACADEMIC_YEAR__SEMESTERS = eINSTANCE.getAcademicYear_Semesters();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.SemesterImpl <em>Semester</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.SemesterImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getSemester()
		 * @generated
		 */
		EClass SEMESTER = eINSTANCE.getSemester();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__YEAR = eINSTANCE.getSemester_Year();

		/**
		 * The meta object literal for the '<em><b>Max Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__MAX_CREDITS = eINSTANCE.getSemester_MaxCredits();

		/**
		 * The meta object literal for the '<em><b>Semester Season</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__SEMESTER_SEASON = eINSTANCE.getSemester_SemesterSeason();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.SpecializationImpl <em>Specialization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.SpecializationImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getSpecialization()
		 * @generated
		 */
		EClass SPECIALIZATION = eINSTANCE.getSpecialization();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPECIALIZATION__NAME = eINSTANCE.getSpecialization_Name();

		/**
		 * The meta object literal for the '<em><b>Programme</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPECIALIZATION__PROGRAMME = eINSTANCE.getSpecialization_Programme();

		/**
		 * The meta object literal for the '<em><b>Years</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPECIALIZATION__YEARS = eINSTANCE.getSpecialization_Years();

		/**
		 * The meta object literal for the '<em><b>Course Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__COURSE_GROUPS = eINSTANCE.getSemester_CourseGroups();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.CourseImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Running Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__RUNNING_SEMESTER = eINSTANCE.getCourse_RunningSemester();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__DURATION = eINSTANCE.getCourse_Duration();

		/**
		 * The meta object literal for the '<em><b>Credits Per Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS_PER_SEMESTER = eINSTANCE.getCourse_CreditsPerSemester();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.CourseGroupImpl <em>Course Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.CourseGroupImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getCourseGroup()
		 * @generated
		 */
		EClass COURSE_GROUP = eINSTANCE.getCourseGroup();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_GROUP__SEMESTER = eINSTANCE.getCourseGroup_Semester();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_GROUP__COURSES = eINSTANCE.getCourseGroup_Courses();

		/**
		 * The meta object literal for the '<em><b>Max Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_GROUP__MAX_CREDITS = eINSTANCE.getCourseGroup_MaxCredits();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_GROUP__NAME = eINSTANCE.getCourseGroup_Name();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.DepartmentImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSES = eINSTANCE.getDepartment_Courses();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Studies</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__STUDIES = eINSTANCE.getDepartment_Studies();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.StudyImpl <em>Study</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.StudyImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getStudy()
		 * @generated
		 */
		EClass STUDY = eINSTANCE.getStudy();

		/**
		 * The meta object literal for the '<em><b>Programmes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY__PROGRAMMES = eINSTANCE.getStudy_Programmes();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY__NAME = eINSTANCE.getStudy_Name();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY__CODE = eINSTANCE.getStudy_Code();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY__TYPE = eINSTANCE.getStudy_Type();

		/**
		 * The meta object literal for the '<em><b>Duration Years</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY__DURATION_YEARS = eINSTANCE.getStudy_DurationYears();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.ObligatoryPickCourseGroupImpl <em>Obligatory Pick Course Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.ObligatoryPickCourseGroupImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getObligatoryPickCourseGroup()
		 * @generated
		 */
		EClass OBLIGATORY_PICK_COURSE_GROUP = eINSTANCE.getObligatoryPickCourseGroup();

		/**
		 * The meta object literal for the '<em><b>Course Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBLIGATORY_PICK_COURSE_GROUP__COURSE_COUNT = eINSTANCE.getObligatoryPickCourseGroup_CourseCount();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBLIGATORY_PICK_COURSE_GROUP___GET_NAME = eINSTANCE.getObligatoryPickCourseGroup__GetName();

		/**
		 * The meta object literal for the '{@link StudyPlan.impl.FreeChoiceCourseGroupImpl <em>Free Choice Course Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.impl.FreeChoiceCourseGroupImpl
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getFreeChoiceCourseGroup()
		 * @generated
		 */
		EClass FREE_CHOICE_COURSE_GROUP = eINSTANCE.getFreeChoiceCourseGroup();

		/**
		 * The meta object literal for the '<em><b>Guaranteed Non Colliding</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_CHOICE_COURSE_GROUP__GUARANTEED_NON_COLLIDING = eINSTANCE.getFreeChoiceCourseGroup_GuaranteedNonColliding();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FREE_CHOICE_COURSE_GROUP___GET_NAME = eINSTANCE.getFreeChoiceCourseGroup__GetName();

		/**
		 * The meta object literal for the '{@link StudyPlan.ESemester <em>ESemester</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.ESemester
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getESemester()
		 * @generated
		 */
		EEnum ESEMESTER = eINSTANCE.getESemester();

		/**
		 * The meta object literal for the '{@link StudyPlan.EProgramType <em>EProgram Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see StudyPlan.EProgramType
		 * @see StudyPlan.impl.StudyPlanPackageImpl#getEProgramType()
		 * @generated
		 */
		EEnum EPROGRAM_TYPE = eINSTANCE.getEProgramType();

	}

} //StudyPlanPackage
