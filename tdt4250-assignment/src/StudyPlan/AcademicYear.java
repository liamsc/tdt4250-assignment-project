/**
 */
package StudyPlan;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Academic Year</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.AcademicYear#getProgramme <em>Programme</em>}</li>
 *   <li>{@link StudyPlan.AcademicYear#getStartYear <em>Start Year</em>}</li>
 *   <li>{@link StudyPlan.AcademicYear#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link StudyPlan.AcademicYear#getSemesters <em>Semesters</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getAcademicYear()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='cannotHaveTwoSemestersOfSameSeason'"
 * @generated
 */
public interface AcademicYear extends EObject {
	/**
	 * Returns the value of the '<em><b>Programme</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Programme#getCommonYears <em>Common Years</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programme</em>' container reference.
	 * @see #setProgramme(Programme)
	 * @see StudyPlan.StudyPlanPackage#getAcademicYear_Programme()
	 * @see StudyPlan.Programme#getCommonYears
	 * @model opposite="commonYears" transient="false"
	 * @generated
	 */
	Programme getProgramme();

	/**
	 * Sets the value of the '{@link StudyPlan.AcademicYear#getProgramme <em>Programme</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Programme</em>' container reference.
	 * @see #getProgramme()
	 * @generated
	 */
	void setProgramme(Programme value);

	/**
	 * Returns the value of the '<em><b>Start Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Year</em>' attribute.
	 * @see #setStartYear(int)
	 * @see StudyPlan.StudyPlanPackage#getAcademicYear_StartYear()
	 * @model
	 * @generated
	 */
	int getStartYear();

	/**
	 * Sets the value of the '{@link StudyPlan.AcademicYear#getStartYear <em>Start Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Year</em>' attribute.
	 * @see #getStartYear()
	 * @generated
	 */
	void setStartYear(int value);

	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Specialization#getYears <em>Years</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' container reference.
	 * @see #setSpecialization(Specialization)
	 * @see StudyPlan.StudyPlanPackage#getAcademicYear_Specialization()
	 * @see StudyPlan.Specialization#getYears
	 * @model opposite="years" transient="false"
	 * @generated
	 */
	Specialization getSpecialization();

	/**
	 * Sets the value of the '{@link StudyPlan.AcademicYear#getSpecialization <em>Specialization</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialization</em>' container reference.
	 * @see #getSpecialization()
	 * @generated
	 */
	void setSpecialization(Specialization value);

	/**
	 * Returns the value of the '<em><b>Semesters</b></em>' containment reference list.
	 * The list contents are of type {@link StudyPlan.Semester}.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Semester#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semesters</em>' containment reference list.
	 * @see StudyPlan.StudyPlanPackage#getAcademicYear_Semesters()
	 * @see StudyPlan.Semester#getYear
	 * @model opposite="year" containment="true" required="true" upper="2"
	 * @generated
	 */
	EList<Semester> getSemesters();

} // AcademicYear
