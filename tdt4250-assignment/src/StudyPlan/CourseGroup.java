/**
 */
package StudyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.CourseGroup#getSemester <em>Semester</em>}</li>
 *   <li>{@link StudyPlan.CourseGroup#getCourses <em>Courses</em>}</li>
 *   <li>{@link StudyPlan.CourseGroup#getMaxCredits <em>Max Credits</em>}</li>
 *   <li>{@link StudyPlan.CourseGroup#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getCourseGroup()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 courseGroupCannotBeEmpty='self.courses-&gt;size() &gt; 0'"
 * @generated
 */
public interface CourseGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Semester</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Semester#getCourseGroups <em>Course Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' container reference.
	 * @see #setSemester(Semester)
	 * @see StudyPlan.StudyPlanPackage#getCourseGroup_Semester()
	 * @see StudyPlan.Semester#getCourseGroups
	 * @model opposite="courseGroups" required="true" transient="false"
	 * @generated
	 */
	Semester getSemester();

	/**
	 * Sets the value of the '{@link StudyPlan.CourseGroup#getSemester <em>Semester</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' container reference.
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(Semester value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' reference list.
	 * The list contents are of type {@link StudyPlan.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' reference list.
	 * @see StudyPlan.StudyPlanPackage#getCourseGroup_Courses()
	 * @model
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Max Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Credits</em>' attribute.
	 * @see StudyPlan.StudyPlanPackage#getCourseGroup_MaxCredits()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	float getMaxCredits();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see StudyPlan.StudyPlanPackage#getCourseGroup_Name()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

} // CourseGroup
