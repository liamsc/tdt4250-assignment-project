/**
 */
package StudyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Programme</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.Programme#getStartYear <em>Start Year</em>}</li>
 *   <li>{@link StudyPlan.Programme#getCommonYears <em>Common Years</em>}</li>
 *   <li>{@link StudyPlan.Programme#getSpecializations <em>Specializations</em>}</li>
 *   <li>{@link StudyPlan.Programme#getSpecializationStartYearOffset <em>Specialization Start Year Offset</em>}</li>
 *   <li>{@link StudyPlan.Programme#getStudy <em>Study</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getProgramme()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='needsSameAmountOfYearsAsProgrammeType specializationsMustHaveCorrectLength correctNumberOfBaseYears'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 specializationStartYearOffsetMustBeValid='self.specializationStartYearOffset &gt; 0 and self.specializationStartYearOffset &lt; self.study.durationYears -1'"
 * @generated
 */
public interface Programme extends EObject {
	/**
	 * Returns the value of the '<em><b>Start Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Year</em>' attribute.
	 * @see #setStartYear(int)
	 * @see StudyPlan.StudyPlanPackage#getProgramme_StartYear()
	 * @model
	 * @generated
	 */
	int getStartYear();

	/**
	 * Sets the value of the '{@link StudyPlan.Programme#getStartYear <em>Start Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Year</em>' attribute.
	 * @see #getStartYear()
	 * @generated
	 */
	void setStartYear(int value);

	/**
	 * Returns the value of the '<em><b>Common Years</b></em>' containment reference list.
	 * The list contents are of type {@link StudyPlan.AcademicYear}.
	 * It is bidirectional and its opposite is '{@link StudyPlan.AcademicYear#getProgramme <em>Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Common Years</em>' containment reference list.
	 * @see StudyPlan.StudyPlanPackage#getProgramme_CommonYears()
	 * @see StudyPlan.AcademicYear#getProgramme
	 * @model opposite="programme" containment="true"
	 * @generated
	 */
	EList<AcademicYear> getCommonYears();

	/**
	 * Returns the value of the '<em><b>Specializations</b></em>' containment reference list.
	 * The list contents are of type {@link StudyPlan.Specialization}.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Specialization#getProgramme <em>Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specializations</em>' containment reference list.
	 * @see StudyPlan.StudyPlanPackage#getProgramme_Specializations()
	 * @see StudyPlan.Specialization#getProgramme
	 * @model opposite="programme" containment="true"
	 * @generated
	 */
	EList<Specialization> getSpecializations();

	/**
	 * Returns the value of the '<em><b>Specialization Start Year Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization Start Year Offset</em>' attribute.
	 * @see #setSpecializationStartYearOffset(int)
	 * @see StudyPlan.StudyPlanPackage#getProgramme_SpecializationStartYearOffset()
	 * @model
	 * @generated
	 */
	int getSpecializationStartYearOffset();

	/**
	 * Sets the value of the '{@link StudyPlan.Programme#getSpecializationStartYearOffset <em>Specialization Start Year Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialization Start Year Offset</em>' attribute.
	 * @see #getSpecializationStartYearOffset()
	 * @generated
	 */
	void setSpecializationStartYearOffset(int value);

	/**
	 * Returns the value of the '<em><b>Study</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Study#getProgrammes <em>Programmes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study</em>' container reference.
	 * @see #setStudy(Study)
	 * @see StudyPlan.StudyPlanPackage#getProgramme_Study()
	 * @see StudyPlan.Study#getProgrammes
	 * @model opposite="programmes" required="true" transient="false"
	 * @generated
	 */
	Study getStudy();

	/**
	 * Sets the value of the '{@link StudyPlan.Programme#getStudy <em>Study</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Study</em>' container reference.
	 * @see #getStudy()
	 * @generated
	 */
	void setStudy(Study value);

} // Programme
