/**
 */
package StudyPlan;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obligatory Pick Course Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.ObligatoryPickCourseGroup#getCourseCount <em>Course Count</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getObligatoryPickCourseGroup()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='isValidCourseCountRequirement hasEnoughCoursesForItsOwnConstraint'"
 * @generated
 */
public interface ObligatoryPickCourseGroup extends CourseGroup {
	/**
	 * Returns the value of the '<em><b>Course Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Count</em>' attribute.
	 * @see #setCourseCount(int)
	 * @see StudyPlan.StudyPlanPackage#getObligatoryPickCourseGroup_CourseCount()
	 * @model
	 * @generated
	 */
	int getCourseCount();

	/**
	 * Sets the value of the '{@link StudyPlan.ObligatoryPickCourseGroup#getCourseCount <em>Course Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Count</em>' attribute.
	 * @see #getCourseCount()
	 * @generated
	 */
	void setCourseCount(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getName();

} // ObligatoryPickCourseGroup
