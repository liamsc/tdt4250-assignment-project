/**
 */
package StudyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.Study#getProgrammes <em>Programmes</em>}</li>
 *   <li>{@link StudyPlan.Study#getName <em>Name</em>}</li>
 *   <li>{@link StudyPlan.Study#getCode <em>Code</em>}</li>
 *   <li>{@link StudyPlan.Study#getType <em>Type</em>}</li>
 *   <li>{@link StudyPlan.Study#getDurationYears <em>Duration Years</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getStudy()
 * @model
 * @generated
 */
public interface Study extends EObject {
	/**
	 * Returns the value of the '<em><b>Programmes</b></em>' containment reference list.
	 * The list contents are of type {@link StudyPlan.Programme}.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Programme#getStudy <em>Study</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programmes</em>' containment reference list.
	 * @see StudyPlan.StudyPlanPackage#getStudy_Programmes()
	 * @see StudyPlan.Programme#getStudy
	 * @model opposite="study" containment="true"
	 * @generated
	 */
	EList<Programme> getProgrammes();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see StudyPlan.StudyPlanPackage#getStudy_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link StudyPlan.Study#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see StudyPlan.StudyPlanPackage#getStudy_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link StudyPlan.Study#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link StudyPlan.EProgramType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see StudyPlan.EProgramType
	 * @see #setType(EProgramType)
	 * @see StudyPlan.StudyPlanPackage#getStudy_Type()
	 * @model
	 * @generated
	 */
	EProgramType getType();

	/**
	 * Sets the value of the '{@link StudyPlan.Study#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see StudyPlan.EProgramType
	 * @see #getType()
	 * @generated
	 */
	void setType(EProgramType value);

	/**
	 * Returns the value of the '<em><b>Duration Years</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration Years</em>' attribute.
	 * @see StudyPlan.StudyPlanPackage#getStudy_DurationYears()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getDurationYears();

} // Study
