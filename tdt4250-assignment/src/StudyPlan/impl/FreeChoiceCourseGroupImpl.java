/**
 */
package StudyPlan.impl;

import StudyPlan.FreeChoiceCourseGroup;
import StudyPlan.StudyPlanPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Free Choice Course Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.impl.FreeChoiceCourseGroupImpl#isGuaranteedNonColliding <em>Guaranteed Non Colliding</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FreeChoiceCourseGroupImpl extends CourseGroupImpl implements FreeChoiceCourseGroup {
	/**
	 * The default value of the '{@link #isGuaranteedNonColliding() <em>Guaranteed Non Colliding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGuaranteedNonColliding()
	 * @generated
	 * @ordered
	 */
	protected static final boolean GUARANTEED_NON_COLLIDING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isGuaranteedNonColliding() <em>Guaranteed Non Colliding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGuaranteedNonColliding()
	 * @generated
	 * @ordered
	 */
	protected boolean guaranteedNonColliding = GUARANTEED_NON_COLLIDING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FreeChoiceCourseGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyPlanPackage.Literals.FREE_CHOICE_COURSE_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isGuaranteedNonColliding() {
		return guaranteedNonColliding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGuaranteedNonColliding(boolean newGuaranteedNonColliding) {
		boolean oldGuaranteedNonColliding = guaranteedNonColliding;
		guaranteedNonColliding = newGuaranteedNonColliding;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.FREE_CHOICE_COURSE_GROUP__GUARANTEED_NON_COLLIDING, oldGuaranteedNonColliding, guaranteedNonColliding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyPlanPackage.FREE_CHOICE_COURSE_GROUP__GUARANTEED_NON_COLLIDING:
				return isGuaranteedNonColliding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyPlanPackage.FREE_CHOICE_COURSE_GROUP__GUARANTEED_NON_COLLIDING:
				setGuaranteedNonColliding((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.FREE_CHOICE_COURSE_GROUP__GUARANTEED_NON_COLLIDING:
				setGuaranteedNonColliding(GUARANTEED_NON_COLLIDING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.FREE_CHOICE_COURSE_GROUP__GUARANTEED_NON_COLLIDING:
				return guaranteedNonColliding != GUARANTEED_NON_COLLIDING_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (guaranteedNonColliding: ");
		result.append(guaranteedNonColliding);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 */
	@Override
	public String getName() {
		return this.isGuaranteedNonColliding() ? "VA" : "VB";
	}

} //FreeChoiceCourseGroupImpl
