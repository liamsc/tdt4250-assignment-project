/**
 */
package StudyPlan.impl;

import StudyPlan.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StudyPlanFactoryImpl extends EFactoryImpl implements StudyPlanFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StudyPlanFactory init() {
		try {
			StudyPlanFactory theStudyPlanFactory = (StudyPlanFactory)EPackage.Registry.INSTANCE.getEFactory(StudyPlanPackage.eNS_URI);
			if (theStudyPlanFactory != null) {
				return theStudyPlanFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StudyPlanFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyPlanFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StudyPlanPackage.PROGRAMME: return createProgramme();
			case StudyPlanPackage.ACADEMIC_YEAR: return createAcademicYear();
			case StudyPlanPackage.SEMESTER: return createSemester();
			case StudyPlanPackage.SPECIALIZATION: return createSpecialization();
			case StudyPlanPackage.COURSE: return createCourse();
			case StudyPlanPackage.DEPARTMENT: return createDepartment();
			case StudyPlanPackage.STUDY: return createStudy();
			case StudyPlanPackage.OBLIGATORY_PICK_COURSE_GROUP: return createObligatoryPickCourseGroup();
			case StudyPlanPackage.FREE_CHOICE_COURSE_GROUP: return createFreeChoiceCourseGroup();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case StudyPlanPackage.ESEMESTER:
				return createESemesterFromString(eDataType, initialValue);
			case StudyPlanPackage.EPROGRAM_TYPE:
				return createEProgramTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case StudyPlanPackage.ESEMESTER:
				return convertESemesterToString(eDataType, instanceValue);
			case StudyPlanPackage.EPROGRAM_TYPE:
				return convertEProgramTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Programme createProgramme() {
		ProgrammeImpl programme = new ProgrammeImpl();
		return programme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AcademicYear createAcademicYear() {
		AcademicYearImpl academicYear = new AcademicYearImpl();
		return academicYear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Semester createSemester() {
		SemesterImpl semester = new SemesterImpl();
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Specialization createSpecialization() {
		SpecializationImpl specialization = new SpecializationImpl();
		return specialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Department createDepartment() {
		DepartmentImpl department = new DepartmentImpl();
		return department;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Study createStudy() {
		StudyImpl study = new StudyImpl();
		return study;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ObligatoryPickCourseGroup createObligatoryPickCourseGroup() {
		ObligatoryPickCourseGroupImpl obligatoryPickCourseGroup = new ObligatoryPickCourseGroupImpl();
		return obligatoryPickCourseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FreeChoiceCourseGroup createFreeChoiceCourseGroup() {
		FreeChoiceCourseGroupImpl freeChoiceCourseGroup = new FreeChoiceCourseGroupImpl();
		return freeChoiceCourseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ESemester createESemesterFromString(EDataType eDataType, String initialValue) {
		ESemester result = ESemester.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertESemesterToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EProgramType createEProgramTypeFromString(EDataType eDataType, String initialValue) {
		EProgramType result = EProgramType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEProgramTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StudyPlanPackage getStudyPlanPackage() {
		return (StudyPlanPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StudyPlanPackage getPackage() {
		return StudyPlanPackage.eINSTANCE;
	}

} //StudyPlanFactoryImpl
