/**
 */
package StudyPlan.impl;

import StudyPlan.AcademicYear;
import StudyPlan.Programme;
import StudyPlan.Specialization;
import StudyPlan.Study;
import StudyPlan.StudyPlanPackage;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Programme</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.impl.ProgrammeImpl#getStartYear <em>Start Year</em>}</li>
 *   <li>{@link StudyPlan.impl.ProgrammeImpl#getCommonYears <em>Common Years</em>}</li>
 *   <li>{@link StudyPlan.impl.ProgrammeImpl#getSpecializations <em>Specializations</em>}</li>
 *   <li>{@link StudyPlan.impl.ProgrammeImpl#getSpecializationStartYearOffset <em>Specialization Start Year Offset</em>}</li>
 *   <li>{@link StudyPlan.impl.ProgrammeImpl#getStudy <em>Study</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProgrammeImpl extends MinimalEObjectImpl.Container implements Programme {
	/**
	 * The default value of the '{@link #getStartYear() <em>Start Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartYear()
	 * @generated
	 * @ordered
	 */
	protected static final int START_YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartYear() <em>Start Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartYear()
	 * @generated
	 * @ordered
	 */
	protected int startYear = START_YEAR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCommonYears() <em>Common Years</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommonYears()
	 * @generated
	 * @ordered
	 */
	protected EList<AcademicYear> commonYears;

	/**
	 * The cached value of the '{@link #getSpecializations() <em>Specializations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecializations()
	 * @generated
	 * @ordered
	 */
	protected EList<Specialization> specializations;

	/**
	 * The default value of the '{@link #getSpecializationStartYearOffset() <em>Specialization Start Year Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecializationStartYearOffset()
	 * @generated
	 * @ordered
	 */
	protected static final int SPECIALIZATION_START_YEAR_OFFSET_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSpecializationStartYearOffset() <em>Specialization Start Year Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecializationStartYearOffset()
	 * @generated
	 * @ordered
	 */
	protected int specializationStartYearOffset = SPECIALIZATION_START_YEAR_OFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgrammeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyPlanPackage.Literals.PROGRAMME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStartYear() {
		return startYear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartYear(int newStartYear) {
		int oldStartYear = startYear;
		startYear = newStartYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.PROGRAMME__START_YEAR, oldStartYear, startYear));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AcademicYear> getCommonYears() {
		if (commonYears == null) {
			commonYears = new EObjectContainmentWithInverseEList<AcademicYear>(AcademicYear.class, this, StudyPlanPackage.PROGRAMME__COMMON_YEARS, StudyPlanPackage.ACADEMIC_YEAR__PROGRAMME);
		}
		return commonYears;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Specialization> getSpecializations() {
		if (specializations == null) {
			specializations = new EObjectContainmentWithInverseEList<Specialization>(Specialization.class, this, StudyPlanPackage.PROGRAMME__SPECIALIZATIONS, StudyPlanPackage.SPECIALIZATION__PROGRAMME);
		}
		return specializations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getSpecializationStartYearOffset() {
		return specializationStartYearOffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecializationStartYearOffset(int newSpecializationStartYearOffset) {
		int oldSpecializationStartYearOffset = specializationStartYearOffset;
		specializationStartYearOffset = newSpecializationStartYearOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.PROGRAMME__SPECIALIZATION_START_YEAR_OFFSET, oldSpecializationStartYearOffset, specializationStartYearOffset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Study getStudy() {
		if (eContainerFeatureID() != StudyPlanPackage.PROGRAMME__STUDY) return null;
		return (Study)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStudy(Study newStudy, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newStudy, StudyPlanPackage.PROGRAMME__STUDY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStudy(Study newStudy) {
		if (newStudy != eInternalContainer() || (eContainerFeatureID() != StudyPlanPackage.PROGRAMME__STUDY && newStudy != null)) {
			if (EcoreUtil.isAncestor(this, newStudy))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStudy != null)
				msgs = ((InternalEObject)newStudy).eInverseAdd(this, StudyPlanPackage.STUDY__PROGRAMMES, Study.class, msgs);
			msgs = basicSetStudy(newStudy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.PROGRAMME__STUDY, newStudy, newStudy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.PROGRAMME__COMMON_YEARS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCommonYears()).basicAdd(otherEnd, msgs);
			case StudyPlanPackage.PROGRAMME__SPECIALIZATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSpecializations()).basicAdd(otherEnd, msgs);
			case StudyPlanPackage.PROGRAMME__STUDY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetStudy((Study)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.PROGRAMME__COMMON_YEARS:
				return ((InternalEList<?>)getCommonYears()).basicRemove(otherEnd, msgs);
			case StudyPlanPackage.PROGRAMME__SPECIALIZATIONS:
				return ((InternalEList<?>)getSpecializations()).basicRemove(otherEnd, msgs);
			case StudyPlanPackage.PROGRAMME__STUDY:
				return basicSetStudy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case StudyPlanPackage.PROGRAMME__STUDY:
				return eInternalContainer().eInverseRemove(this, StudyPlanPackage.STUDY__PROGRAMMES, Study.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyPlanPackage.PROGRAMME__START_YEAR:
				return getStartYear();
			case StudyPlanPackage.PROGRAMME__COMMON_YEARS:
				return getCommonYears();
			case StudyPlanPackage.PROGRAMME__SPECIALIZATIONS:
				return getSpecializations();
			case StudyPlanPackage.PROGRAMME__SPECIALIZATION_START_YEAR_OFFSET:
				return getSpecializationStartYearOffset();
			case StudyPlanPackage.PROGRAMME__STUDY:
				return getStudy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyPlanPackage.PROGRAMME__START_YEAR:
				setStartYear((Integer)newValue);
				return;
			case StudyPlanPackage.PROGRAMME__COMMON_YEARS:
				getCommonYears().clear();
				getCommonYears().addAll((Collection<? extends AcademicYear>)newValue);
				return;
			case StudyPlanPackage.PROGRAMME__SPECIALIZATIONS:
				getSpecializations().clear();
				getSpecializations().addAll((Collection<? extends Specialization>)newValue);
				return;
			case StudyPlanPackage.PROGRAMME__SPECIALIZATION_START_YEAR_OFFSET:
				setSpecializationStartYearOffset((Integer)newValue);
				return;
			case StudyPlanPackage.PROGRAMME__STUDY:
				setStudy((Study)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.PROGRAMME__START_YEAR:
				setStartYear(START_YEAR_EDEFAULT);
				return;
			case StudyPlanPackage.PROGRAMME__COMMON_YEARS:
				getCommonYears().clear();
				return;
			case StudyPlanPackage.PROGRAMME__SPECIALIZATIONS:
				getSpecializations().clear();
				return;
			case StudyPlanPackage.PROGRAMME__SPECIALIZATION_START_YEAR_OFFSET:
				setSpecializationStartYearOffset(SPECIALIZATION_START_YEAR_OFFSET_EDEFAULT);
				return;
			case StudyPlanPackage.PROGRAMME__STUDY:
				setStudy((Study)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.PROGRAMME__START_YEAR:
				return startYear != START_YEAR_EDEFAULT;
			case StudyPlanPackage.PROGRAMME__COMMON_YEARS:
				return commonYears != null && !commonYears.isEmpty();
			case StudyPlanPackage.PROGRAMME__SPECIALIZATIONS:
				return specializations != null && !specializations.isEmpty();
			case StudyPlanPackage.PROGRAMME__SPECIALIZATION_START_YEAR_OFFSET:
				return specializationStartYearOffset != SPECIALIZATION_START_YEAR_OFFSET_EDEFAULT;
			case StudyPlanPackage.PROGRAMME__STUDY:
				return getStudy() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startYear: ");
		result.append(startYear);
		result.append(", specializationStartYearOffset: ");
		result.append(specializationStartYearOffset);
		result.append(')');
		return result.toString();
	}

} //ProgrammeImpl
