/**
 */
package StudyPlan.impl;

import StudyPlan.CourseGroup;
import StudyPlan.ObligatoryPickCourseGroup;
import StudyPlan.StudyPlanPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obligatory Pick Course Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.impl.ObligatoryPickCourseGroupImpl#getCourseCount <em>Course Count</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObligatoryPickCourseGroupImpl extends CourseGroupImpl implements ObligatoryPickCourseGroup {
	/**
	 * The default value of the '{@link #getCourseCount() <em>Course Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCount()
	 * @generated
	 * @ordered
	 */
	protected static final int COURSE_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCourseCount() <em>Course Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCount()
	 * @generated
	 * @ordered
	 */
	protected int courseCount = COURSE_COUNT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObligatoryPickCourseGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyPlanPackage.Literals.OBLIGATORY_PICK_COURSE_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCourseCount() {
		return courseCount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCourseCount(int newCourseCount) {
		int oldCourseCount = courseCount;
		courseCount = newCourseCount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.OBLIGATORY_PICK_COURSE_GROUP__COURSE_COUNT, oldCourseCount, courseCount));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyPlanPackage.OBLIGATORY_PICK_COURSE_GROUP__COURSE_COUNT:
				return getCourseCount();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyPlanPackage.OBLIGATORY_PICK_COURSE_GROUP__COURSE_COUNT:
				setCourseCount((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.OBLIGATORY_PICK_COURSE_GROUP__COURSE_COUNT:
				setCourseCount(COURSE_COUNT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.OBLIGATORY_PICK_COURSE_GROUP__COURSE_COUNT:
				return courseCount != COURSE_COUNT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (courseCount: ");
		result.append(courseCount);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	@Override
	public String getName() {
		if(this.getCourseCount() == 0) {
			return "O";
		} else {
			int positionInList = 0;
			// Calcualte which position in the list of course groups this course group is in.
			// Assumes the iterator which is returned by getCourseGroups stays the same as long as the order is the same in XML.
			for(CourseGroup group : this.getSemester().getCourseGroups()) {
				if(group instanceof ObligatoryPickCourseGroup) {
					if(group == this) {
						break;
					} else {
						positionInList++;
					}
				}
			}
			return "M" + this.getCourseCount() + (char)('A'+positionInList);
		}
	}

} //ObligatoryPickCourseGroupImpl
