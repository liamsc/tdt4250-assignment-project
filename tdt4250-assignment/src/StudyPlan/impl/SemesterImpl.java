/**
 */
package StudyPlan.impl;

import StudyPlan.AcademicYear;
import StudyPlan.CourseGroup;
import StudyPlan.ESemester;
import StudyPlan.Semester;
import StudyPlan.StudyPlanPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.impl.SemesterImpl#getCourseGroups <em>Course Groups</em>}</li>
 *   <li>{@link StudyPlan.impl.SemesterImpl#getYear <em>Year</em>}</li>
 *   <li>{@link StudyPlan.impl.SemesterImpl#getSemesterSeason <em>Semester Season</em>}</li>
 *   <li>{@link StudyPlan.impl.SemesterImpl#getMaxCredits <em>Max Credits</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SemesterImpl extends MinimalEObjectImpl.Container implements Semester {
	/**
	 * The cached value of the '{@link #getCourseGroups() <em>Course Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseGroup> courseGroups;

	/**
	 * The default value of the '{@link #getSemesterSeason() <em>Semester Season</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterSeason()
	 * @generated
	 * @ordered
	 */
	protected static final ESemester SEMESTER_SEASON_EDEFAULT = ESemester.SPRING;

	/**
	 * The cached value of the '{@link #getSemesterSeason() <em>Semester Season</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterSeason()
	 * @generated
	 * @ordered
	 */
	protected ESemester semesterSeason = SEMESTER_SEASON_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxCredits() <em>Max Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxCredits()
	 * @generated
	 * @ordered
	 */
	protected static final float MAX_CREDITS_EDEFAULT = 0.0F;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyPlanPackage.Literals.SEMESTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CourseGroup> getCourseGroups() {
		if (courseGroups == null) {
			courseGroups = new EObjectContainmentWithInverseEList<CourseGroup>(CourseGroup.class, this, StudyPlanPackage.SEMESTER__COURSE_GROUPS, StudyPlanPackage.COURSE_GROUP__SEMESTER);
		}
		return courseGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AcademicYear getYear() {
		if (eContainerFeatureID() != StudyPlanPackage.SEMESTER__YEAR) return null;
		return (AcademicYear)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetYear(AcademicYear newYear, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newYear, StudyPlanPackage.SEMESTER__YEAR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYear(AcademicYear newYear) {
		if (newYear != eInternalContainer() || (eContainerFeatureID() != StudyPlanPackage.SEMESTER__YEAR && newYear != null)) {
			if (EcoreUtil.isAncestor(this, newYear))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newYear != null)
				msgs = ((InternalEObject)newYear).eInverseAdd(this, StudyPlanPackage.ACADEMIC_YEAR__SEMESTERS, AcademicYear.class, msgs);
			msgs = basicSetYear(newYear, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.SEMESTER__YEAR, newYear, newYear));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public float getMaxCredits() {
		float sum = 0.0f;
		for(CourseGroup cg : this.getCourseGroups()) {
			sum += cg.getMaxCredits();
		}
		return sum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ESemester getSemesterSeason() {
		return semesterSeason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemesterSeason(ESemester newSemesterSeason) {
		ESemester oldSemesterSeason = semesterSeason;
		semesterSeason = newSemesterSeason == null ? SEMESTER_SEASON_EDEFAULT : newSemesterSeason;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyPlanPackage.SEMESTER__SEMESTER_SEASON, oldSemesterSeason, semesterSeason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__COURSE_GROUPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourseGroups()).basicAdd(otherEnd, msgs);
			case StudyPlanPackage.SEMESTER__YEAR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetYear((AcademicYear)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__COURSE_GROUPS:
				return ((InternalEList<?>)getCourseGroups()).basicRemove(otherEnd, msgs);
			case StudyPlanPackage.SEMESTER__YEAR:
				return basicSetYear(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case StudyPlanPackage.SEMESTER__YEAR:
				return eInternalContainer().eInverseRemove(this, StudyPlanPackage.ACADEMIC_YEAR__SEMESTERS, AcademicYear.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__COURSE_GROUPS:
				return getCourseGroups();
			case StudyPlanPackage.SEMESTER__YEAR:
				return getYear();
			case StudyPlanPackage.SEMESTER__SEMESTER_SEASON:
				return getSemesterSeason();
			case StudyPlanPackage.SEMESTER__MAX_CREDITS:
				return getMaxCredits();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__COURSE_GROUPS:
				getCourseGroups().clear();
				getCourseGroups().addAll((Collection<? extends CourseGroup>)newValue);
				return;
			case StudyPlanPackage.SEMESTER__YEAR:
				setYear((AcademicYear)newValue);
				return;
			case StudyPlanPackage.SEMESTER__SEMESTER_SEASON:
				setSemesterSeason((ESemester)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__COURSE_GROUPS:
				getCourseGroups().clear();
				return;
			case StudyPlanPackage.SEMESTER__YEAR:
				setYear((AcademicYear)null);
				return;
			case StudyPlanPackage.SEMESTER__SEMESTER_SEASON:
				setSemesterSeason(SEMESTER_SEASON_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyPlanPackage.SEMESTER__COURSE_GROUPS:
				return courseGroups != null && !courseGroups.isEmpty();
			case StudyPlanPackage.SEMESTER__YEAR:
				return getYear() != null;
			case StudyPlanPackage.SEMESTER__SEMESTER_SEASON:
				return semesterSeason != SEMESTER_SEASON_EDEFAULT;
			case StudyPlanPackage.SEMESTER__MAX_CREDITS:
				return getMaxCredits() != MAX_CREDITS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semesterSeason: ");
		result.append(semesterSeason);
		result.append(')');
		return result.toString();
	}

} //SemesterImpl
