/**
 */
package StudyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.Specialization#getName <em>Name</em>}</li>
 *   <li>{@link StudyPlan.Specialization#getProgramme <em>Programme</em>}</li>
 *   <li>{@link StudyPlan.Specialization#getYears <em>Years</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getSpecialization()
 * @model annotation="http://www.eclipse.org/acceleo/query/1.0 yearsMustBeCorrect='self.programme.study.durationYears - self.programme.specializationStartYearOffset == self.programme.study.durationYears'"
 * @generated
 */
public interface Specialization extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see StudyPlan.StudyPlanPackage#getSpecialization_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link StudyPlan.Specialization#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Programme</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link StudyPlan.Programme#getSpecializations <em>Specializations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programme</em>' container reference.
	 * @see #setProgramme(Programme)
	 * @see StudyPlan.StudyPlanPackage#getSpecialization_Programme()
	 * @see StudyPlan.Programme#getSpecializations
	 * @model opposite="specializations" required="true" transient="false"
	 * @generated
	 */
	Programme getProgramme();

	/**
	 * Sets the value of the '{@link StudyPlan.Specialization#getProgramme <em>Programme</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Programme</em>' container reference.
	 * @see #getProgramme()
	 * @generated
	 */
	void setProgramme(Programme value);

	/**
	 * Returns the value of the '<em><b>Years</b></em>' containment reference list.
	 * The list contents are of type {@link StudyPlan.AcademicYear}.
	 * It is bidirectional and its opposite is '{@link StudyPlan.AcademicYear#getSpecialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Years</em>' containment reference list.
	 * @see StudyPlan.StudyPlanPackage#getSpecialization_Years()
	 * @see StudyPlan.AcademicYear#getSpecialization
	 * @model opposite="specialization" containment="true" required="true"
	 * @generated
	 */
	EList<AcademicYear> getYears();

} // Specialization
