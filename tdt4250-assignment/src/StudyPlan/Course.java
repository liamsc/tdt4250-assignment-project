/**
 */
package StudyPlan;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.Course#getCode <em>Code</em>}</li>
 *   <li>{@link StudyPlan.Course#getName <em>Name</em>}</li>
 *   <li>{@link StudyPlan.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link StudyPlan.Course#getRunningSemester <em>Running Semester</em>}</li>
 *   <li>{@link StudyPlan.Course#getDuration <em>Duration</em>}</li>
 *   <li>{@link StudyPlan.Course#getCreditsPerSemester <em>Credits Per Semester</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see StudyPlan.StudyPlanPackage#getCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link StudyPlan.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see StudyPlan.StudyPlanPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link StudyPlan.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(float)
	 * @see StudyPlan.StudyPlanPackage#getCourse_Credits()
	 * @model
	 * @generated
	 */
	float getCredits();

	/**
	 * Sets the value of the '{@link StudyPlan.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(float value);

	/**
	 * Returns the value of the '<em><b>Running Semester</b></em>' attribute.
	 * The literals are from the enumeration {@link StudyPlan.ESemester}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Running Semester</em>' attribute.
	 * @see StudyPlan.ESemester
	 * @see #setRunningSemester(ESemester)
	 * @see StudyPlan.StudyPlanPackage#getCourse_RunningSemester()
	 * @model
	 * @generated
	 */
	ESemester getRunningSemester();

	/**
	 * Sets the value of the '{@link StudyPlan.Course#getRunningSemester <em>Running Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Running Semester</em>' attribute.
	 * @see StudyPlan.ESemester
	 * @see #getRunningSemester()
	 * @generated
	 */
	void setRunningSemester(ESemester value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(int)
	 * @see StudyPlan.StudyPlanPackage#getCourse_Duration()
	 * @model
	 * @generated
	 */
	int getDuration();

	/**
	 * Sets the value of the '{@link StudyPlan.Course#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(int value);

	/**
	 * Returns the value of the '<em><b>Credits Per Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits Per Semester</em>' attribute.
	 * @see StudyPlan.StudyPlanPackage#getCourse_CreditsPerSemester()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	float getCreditsPerSemester();

} // Course
