/**
 */
package StudyPlan;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link StudyPlan.Semester#getCourseGroups <em>Course Groups</em>}</li>
 *   <li>{@link StudyPlan.Semester#getYear <em>Year</em>}</li>
 *   <li>{@link StudyPlan.Semester#getSemesterSeason <em>Semester Season</em>}</li>
 *   <li>{@link StudyPlan.Semester#getMaxCredits <em>Max Credits</em>}</li>
 * </ul>
 *
 * @see StudyPlan.StudyPlanPackage#getSemester()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='maxOneObligatoryGroup maxOneOfEachFreeChoiceGroup'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 semesterMustBeAbleToAchieve30Credits='self.maxCredits &gt;= 30'"
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Year</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link StudyPlan.AcademicYear#getSemesters <em>Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' container reference.
	 * @see #setYear(AcademicYear)
	 * @see StudyPlan.StudyPlanPackage#getSemester_Year()
	 * @see StudyPlan.AcademicYear#getSemesters
	 * @model opposite="semesters" transient="false"
	 * @generated
	 */
	AcademicYear getYear();

	/**
	 * Sets the value of the '{@link StudyPlan.Semester#getYear <em>Year</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' container reference.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(AcademicYear value);

	/**
	 * Returns the value of the '<em><b>Max Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Credits</em>' attribute.
	 * @see StudyPlan.StudyPlanPackage#getSemester_MaxCredits()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	float getMaxCredits();

	/**
	 * Returns the value of the '<em><b>Semester Season</b></em>' attribute.
	 * The literals are from the enumeration {@link StudyPlan.ESemester}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester Season</em>' attribute.
	 * @see StudyPlan.ESemester
	 * @see #setSemesterSeason(ESemester)
	 * @see StudyPlan.StudyPlanPackage#getSemester_SemesterSeason()
	 * @model
	 * @generated
	 */
	ESemester getSemesterSeason();

	/**
	 * Sets the value of the '{@link StudyPlan.Semester#getSemesterSeason <em>Semester Season</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester Season</em>' attribute.
	 * @see StudyPlan.ESemester
	 * @see #getSemesterSeason()
	 * @generated
	 */
	void setSemesterSeason(ESemester value);

	/**
	 * Returns the value of the '<em><b>Course Groups</b></em>' containment reference list.
	 * The list contents are of type {@link StudyPlan.CourseGroup}.
	 * It is bidirectional and its opposite is '{@link StudyPlan.CourseGroup#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Groups</em>' containment reference list.
	 * @see StudyPlan.StudyPlanPackage#getSemester_CourseGroups()
	 * @see StudyPlan.CourseGroup#getSemester
	 * @model opposite="semester" containment="true"
	 * @generated
	 */
	EList<CourseGroup> getCourseGroups();

} // Semester
