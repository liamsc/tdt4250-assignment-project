/**
 */
package StudyPlan.tests;

import StudyPlan.Course;
import StudyPlan.CourseGroup;
import StudyPlan.FreeChoiceCourseGroup;
import StudyPlan.ObligatoryPickCourseGroup;
import StudyPlan.Semester;
import StudyPlan.StudyPlanFactory;
import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Course Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link StudyPlan.CourseGroup#getMaxCredits() <em>Max Credits</em>}</li>
 *   <li>{@link StudyPlan.CourseGroup#getName() <em>Name</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class CourseGroupTest extends TestCase {

	/**
	 * The fixture for this Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseGroup fixture = null;

	/**
	 * Constructs a new Course Group test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseGroupTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CourseGroup fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseGroup getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link StudyPlan.CourseGroup#getMaxCredits() <em>Max Credits</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.CourseGroup#getMaxCredits()
	 * @generated
	 */
	public void testGetMaxCredits() {
		ObligatoryPickCourseGroup obligatory = StudyPlanFactory.eINSTANCE.createObligatoryPickCourseGroup();
		Course obligCourse1 = StudyPlanFactory.eINSTANCE.createCourse();
		obligCourse1.setCredits(7.5f);
		obligCourse1.setDuration(1);
		
		Course obligCourse2 = StudyPlanFactory.eINSTANCE.createCourse();
		obligCourse2.setCredits(10.0f);
		obligCourse2.setDuration(2);
		
		obligatory.getCourses().add(obligCourse1);
		obligatory.getCourses().add(obligCourse2);
		
		assertEquals(7.5f+5.0f, obligatory.getMaxCredits());
		
		//FreeChoiceCourseGroup free = StudyPlanFactory.eINSTANCE.createFreeChoiceCourseGroup();
	}

	/**
	 * Tests the '{@link StudyPlan.CourseGroup#getName() <em>Name</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.CourseGroup#getName()
	 * @generated
	 */
	public void testGetName() {
		ObligatoryPickCourseGroup obligatory1 = StudyPlanFactory.eINSTANCE.createObligatoryPickCourseGroup();
		obligatory1.setCourseCount(0);
		assertEquals("O", obligatory1.getName());
		
		Semester semester = StudyPlanFactory.eINSTANCE.createSemester();
		ObligatoryPickCourseGroup obligatory2 = StudyPlanFactory.eINSTANCE.createObligatoryPickCourseGroup();
		obligatory2.setCourseCount(2);
		semester.getCourseGroups().add(obligatory2);
		
		ObligatoryPickCourseGroup obligatory3 = StudyPlanFactory.eINSTANCE.createObligatoryPickCourseGroup();
		obligatory3.setCourseCount(1);
		semester.getCourseGroups().add(obligatory3);
		
		assertEquals("M2A", obligatory2.getName());
		assertEquals("M1B", obligatory3.getName());
	}

} //CourseGroupTest
