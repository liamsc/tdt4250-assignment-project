/**
 */
package StudyPlan.tests;

import StudyPlan.EProgramType;
import StudyPlan.Study;
import StudyPlan.StudyPlanFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Study</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link StudyPlan.Study#getDurationYears() <em>Duration Years</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StudyTest extends TestCase {

	/**
	 * The fixture for this Study test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Study fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StudyTest.class);
	}

	/**
	 * Constructs a new Study test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Study test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Study fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Study test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Study getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyPlanFactory.eINSTANCE.createStudy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link StudyPlan.Study#getDurationYears() <em>Duration Years</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.Study#getDurationYears()
	 * @generated
	 */
	public void testGetDurationYears() {
		Study bachelor = StudyPlanFactory.eINSTANCE.createStudy();
		bachelor.setType(EProgramType.BACHELOR);
		assertEquals(bachelor.getDurationYears(), 3);
		
		Study master = StudyPlanFactory.eINSTANCE.createStudy();
		master.setType(EProgramType.MASTER);
		assertEquals(master.getDurationYears(), 2);
		
		Study integrated_master = StudyPlanFactory.eINSTANCE.createStudy();
		integrated_master.setType(EProgramType.INTEGRATED_MASTER);
		assertEquals(integrated_master.getDurationYears(), 5);
	}

} //StudyTest
