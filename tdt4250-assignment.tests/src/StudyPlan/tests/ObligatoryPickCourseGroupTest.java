/**
 */
package StudyPlan.tests;

import StudyPlan.ObligatoryPickCourseGroup;
import StudyPlan.StudyPlanFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Obligatory Pick Course Group</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObligatoryPickCourseGroupTest extends CourseGroupTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ObligatoryPickCourseGroupTest.class);
	}

	/**
	 * Constructs a new Obligatory Pick Course Group test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObligatoryPickCourseGroupTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Obligatory Pick Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ObligatoryPickCourseGroup getFixture() {
		return (ObligatoryPickCourseGroup)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyPlanFactory.eINSTANCE.createObligatoryPickCourseGroup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ObligatoryPickCourseGroupTest
