/**
 */
package StudyPlan.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>StudyPlan</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class StudyPlanTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new StudyPlanTests("StudyPlan Tests");
		suite.addTestSuite(SemesterTest.class);
		suite.addTestSuite(CourseTest.class);
		suite.addTestSuite(StudyTest.class);
		suite.addTestSuite(ObligatoryPickCourseGroupTest.class);
		suite.addTestSuite(FreeChoiceCourseGroupTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyPlanTests(String name) {
		super(name);
	}

} //StudyPlanTests
