/**
 */
package StudyPlan.tests;

import StudyPlan.Course;
import StudyPlan.ObligatoryPickCourseGroup;
import StudyPlan.Semester;
import StudyPlan.StudyPlanFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link StudyPlan.Semester#getMaxCredits() <em>Max Credits</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SemesterTest extends TestCase {

	/**
	 * The fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SemesterTest.class);
	}

	/**
	 * Constructs a new Semester test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Semester fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyPlanFactory.eINSTANCE.createSemester());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link StudyPlan.Semester#getMaxCredits() <em>Max Credits</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see StudyPlan.Semester#getMaxCredits()
	 * @generated
	 */
	public void testGetMaxCredits() {
		Semester semester = StudyPlanFactory.eINSTANCE.createSemester();
		
		ObligatoryPickCourseGroup obligatory = StudyPlanFactory.eINSTANCE.createObligatoryPickCourseGroup();
		Course obligCourse1 = StudyPlanFactory.eINSTANCE.createCourse();
		obligCourse1.setCredits(7.5f);
		obligCourse1.setDuration(1);
		
		Course obligCourse2 = StudyPlanFactory.eINSTANCE.createCourse();
		obligCourse2.setCredits(10.0f);
		obligCourse2.setDuration(2);
		
		obligatory.getCourses().add(obligCourse1);
		obligatory.getCourses().add(obligCourse2);
		
		semester.getCourseGroups().add(obligatory);
		
		assertEquals(7.5f+5.0f, semester.getMaxCredits());
	}

} //SemesterTest
