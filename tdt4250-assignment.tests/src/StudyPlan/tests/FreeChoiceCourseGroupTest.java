/**
 */
package StudyPlan.tests;

import StudyPlan.FreeChoiceCourseGroup;
import StudyPlan.StudyPlanFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Free Choice Course Group</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FreeChoiceCourseGroupTest extends CourseGroupTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FreeChoiceCourseGroupTest.class);
	}

	/**
	 * Constructs a new Free Choice Course Group test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeChoiceCourseGroupTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Free Choice Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FreeChoiceCourseGroup getFixture() {
		return (FreeChoiceCourseGroup)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyPlanFactory.eINSTANCE.createFreeChoiceCourseGroup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FreeChoiceCourseGroupTest
